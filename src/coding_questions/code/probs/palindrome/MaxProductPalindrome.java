package coding_questions.code.probs.palindrome;

public class MaxProductPalindrome {

	private static boolean isPalindrome(int no) {
		
		String noStr = "" + no ;
		int j = noStr.length()  - 1;
		for (int i = 0 ; i < noStr.length() / 2 ; i++ , j--) {
			if (noStr.charAt(i) != noStr.charAt(j) )
				return false ;
		}
		
		return true ;
	}
	
	public static void main(String args[]) {
		int maxPalinProd = 0 ;
		for (int i = 999 ; i > 100 ; i-- ) {
			for (int j = 999 ; j > 100 ; j--) {
				
				int prd = i * j ; 
				if (isPalindrome(prd)) {
					if (prd > maxPalinProd) {
						maxPalinProd = prd ;
						break ;
					}
				} else if (prd < maxPalinProd){
					break ;
				}
			}
		}
		
		System.out.println(maxPalinProd);
	}
}
