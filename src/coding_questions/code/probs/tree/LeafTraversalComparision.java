package coding_questions.code.probs.tree;

import java.util.Stack;

/**
 * Leaf Traversal for binary tree is when we traverse the leaves and only the leaves.
 * This problem is do the tree traversal for 2 trees and if they are same return true
 * else return false.
 * 
 *   Note: What happpens the parent might not be same, the structure may be different.
 *   
 *     Tree1:
 *       5
 *    7     8
 *  3   1      9
 *  
 *      Tree 2:
 *      10
 *    2     11
 *  6   5     9
 *    3   1        
 *   
 * @author shaikhjamir
 *
 */
public class LeafTraversalComparision {

	private static class Node {
		int data ;
		Node lc ;
		Node rc ;
		public Node(int data, Node lc, Node rc) {
			this.data = data ;
			this.lc = lc ;
			this.rc = rc ;
		}
	}
	
	public static void main(String args[]) {
		
		Node  n3 = new Node(3, null, null) ;
		Node  n1 = new Node(1, null, null) ;
		Node  n9 = new Node(9, null, null) ;
		
		Node  n7 = new Node(7, n3, n1) ;
		Node  n8 = new Node(8, null, n9) ;
		Node  n5 = new Node(5, n7, n8) ;
		
		Node b3  = new Node(3, null, null) ;
		Node b1  = new Node(1, null, null) ;
		Node b9  = new Node(9, null, null) ;
		
		Node b6  = new Node(6, null, b3) ;
		Node b5  = new Node(5, null, b1) ;
		Node b11  = new Node(11, null, b9) ;
		Node b2  = new Node(2, b6, b5) ;
		Node b10  = new Node(10, b2, b11) ;
		
		System.out.println(isLeafTraversalSame(n5, b10) );
	}
	
	private static boolean isLeafTraversalSame(Node t1, Node t2) {
		
		Stack<Node> s1 = new Stack<>() ;
		Stack<Node> s2 = new Stack<>() ;
		
		s1.push(t1) ;
		s2.push(t2) ;
		
		while (true) {
			
			Node data1 = null ;
			// while find a leaf
			while(s1.isEmpty() == false) {
				Node n = s1.pop() ;
				
				if (n.rc != null )
					s1.push(n.rc) ;
				
				if (n.lc != null )
					s1.push(n.lc) ;
				
				if (n.rc == null && n.lc == null ) {
					data1 = n ;
					break ;
				}
			}
			
			
			Node data2 = null ;
			// while find a leaf
			while(s2.isEmpty() == false) {
				Node n = s2.pop() ;
				
				if (n.rc != null )
					s2.push(n.rc) ;
				
				if (n.lc != null )
					s2.push(n.lc) ;
				
				if (n.rc == null && n.lc == null ) {
					data2 = n ;
					break ;
				}
			}
			
			if (data1 == null && data2 == null )
				return true ;
			
			if (data1 != null && data2 == null )
				return false ;
			if (data1 == null && data2 != null )
				return false ;
			
			if (data1.data != data2.data )
				return false ;
			
			if (s1.empty() && s2.empty())
				break ;
		}
		
		return true ;
	}

}
