package coding_questions.code.probs.strings;

/**
 * Given a String "ADBC"
 * This string has possible permutations as:
 * ABCD, ABDC, ACBD ....
 * Given a String ACBD find the index of this string for the above sorted array.
 * @author shaikhjamir
 *
 */
public class FindStringRankByIndex {

	private static int fact(int f) {
	
		if (f <= 1 )
			return 1 ;
		
		return f * fact( f -1 );
	}
	
	private static int[] arr = new int[26] ;
	
	private static void buildMat(String str) {
		
		for (int i = 0 ; i < 26 ; i++) {
			arr[i] = 0 ;
		}
		
		for (int i = 0 ; i < str.length() ; i++ ) {
			int index = str.charAt(i) - 'A' ;
			arr[index] = arr[index] + 1 ; 
		}
	}
	
	
	private static int getRank(String str, int total) {
		
		if (str == null || str.length() == 0 || str.length() == 1)
			return total ; 
		
		int fact = fact(str.length() - 1 ) ;
		int index = str.charAt(0) - 'A' ;
		int currTotal = 0 ;
		for (int i = 0 ; i < index ; i++ ) {
			if (arr[i] > 0 ) {
				
				currTotal = currTotal + fact * arr[i] ; 
			}
		}
		
		total = total + currTotal ;
		arr[index] = arr[index] - 1 ;
		String subStr = str.substring(1) ;
		return getRank(subStr, total) ;
	}
	
	public static void main(String args[]) {
		
		String org = "QIBSHGTZVWPMCXEFY" ;
		buildMat(org) ;
		System.out.println(getRank("QIBSHGTZVWPMCXEFY", 0));
		
		/*String org = "ABCD" ;
		buildMat(org) ;
		System.out.println(getRank("ABCD", 0));
		buildMat(org) ;
		System.out.println(getRank("ABDC", 0));
		buildMat(org) ;
		System.out.println(getRank("ACBD", 0));
		buildMat(org) ;
		System.out.println(getRank("ACDB", 0));
		buildMat(org) ;
		System.out.println(getRank("ADBC", 0));
		buildMat(org) ;
		System.out.println(getRank("ADCB", 0));
		buildMat(org) ;
		System.out.println(getRank("BACD", 0));
		buildMat(org) ;
		System.out.println(getRank("BADC", 0));
		buildMat(org) ;
		System.out.println(getRank("BCAD", 0));
		buildMat(org) ;
		System.out.println(getRank("BCDA", 0));
		buildMat(org) ;
		System.out.println(getRank("BDAC", 0));
		buildMat(org) ;
		System.out.println(getRank("BDCA", 0));
		buildMat(org) ;
		System.out.println(getRank("CABD", 0));
		buildMat(org) ;
		System.out.println(getRank("CADB", 0));
		buildMat(org) ;
		System.out.println(getRank("CBAD", 0));
		buildMat(org) ;
		System.out.println(getRank("CBDA", 0));
		buildMat(org) ;
		System.out.println(getRank("CDAB", 0));
		buildMat(org) ;
		System.out.println(getRank("CDBA", 0));
		buildMat(org) ;
		System.out.println(getRank("DABC", 0));
		buildMat(org) ;
		System.out.println(getRank("DACB", 0));
		buildMat(org) ;
		System.out.println(getRank("DBAC", 0));
		buildMat(org) ;
		System.out.println(getRank("DBCA", 0));
		buildMat(org) ;
		System.out.println(getRank("DCAB", 0));
		buildMat(org) ;
		System.out.println(getRank("DCBA", 0));*/
	}
}
