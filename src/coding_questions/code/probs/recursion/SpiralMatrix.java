package coding_questions.code.probs.recursion;

/**
 * Given n print the spiral matrix for n
 * 
 * n=3  
 3 3 3 3 3
 3 2 2 2 3
 3 2 1 2 3
 3 2 2 2 3
 3 3 3 3 3
 * @author shaikhjamir
 *
 */
public class SpiralMatrix {

	static int inputSize = 7 ;
	static int matrixSize = 2 * inputSize - 1 ;
	static int[][] matrix = new int[matrixSize][matrixSize] ;
	
	public static void print() {
		
		for (int i = 0 ; i < matrixSize ; i++ ) {
			for (int j = 0 ; j < matrixSize ; j++) {
				
				System.out.print(" " + matrix[i][j]);
			}
			
			System.out.println("");
		}
	}
	
	
	public static void buildSpiralMat(int start, int end, int no) {
		
		System.out.println(start + "==" + end + "==" + no);
		if (no == 1 ) {
			
			matrix[start][end] = no ;
			return ;
		}
		
		for (int i = start ; i <=end ; i++ ) {
			matrix[start][i] = no ;
			matrix[end][i] = no ;
			matrix[i][start] = no ;
			matrix[i][end] = no ;
		}
		
		start++ ;
		end-- ; 
		no-- ;
		buildSpiralMat(start, end, no) ; 
	}
	
	
	public static void main(String args[]) {
		buildSpiralMat(0, matrixSize -1 , inputSize) ; 
		print() ;
	}
}
